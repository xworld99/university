#include </home/egor/CLionProjects/lab3/src/TreeSet.h>
#include <gtest/gtest.h>

class TreeSetTests : public ::testing::Test {
protected:
    TreeSet<int> treeSet;
    void SetUp() override {
        int a[5] = {1, 2, 3, 4, 5};
        treeSet = TreeSet<int>(a, 5);
    }
    void TearDown() override {
    }
};


TEST_F(TreeSetTests, CreateEmpty) {
    //given
    TreeSet<int> treeSet1;
    DynamicArray<int> res = treeSet1.saveToArray();
    //then
    EXPECT_EQ(res.getLength(), 0);
}


TEST_F(TreeSetTests, CreateFromArray) {
    //given
    //when
    DynamicArray<int> res = treeSet.saveToArray();
    //then
    ASSERT_EQ(res.getLength(), 5);
    for (int i = 1; i < 6; i++) {
        EXPECT_EQ(res[i-1], i);
    }
}


TEST_F(TreeSetTests, SaveToArray) {
    //given
    //when
    DynamicArray<int> res = treeSet.saveToArray();
    //then
    ASSERT_EQ(res.getLength(), 5);
    for (int i = 1; i < 6; i++) {
        EXPECT_EQ(res[i-1], i);
    }
}


TEST_F(TreeSetTests, AssignmentOperator) {
    //given
    TreeSet<int> tree1;
    tree1 = treeSet;
    //when
    //then
    for(int i = 1; i < 5; i++) {
        EXPECT_TRUE(tree1.search(i));
    }
}


TEST_F(TreeSetTests, CopyingConstructor) {
    //given
    TreeSet<int> tree1 = treeSet;
    //when
    //then
    for(int i = 1; i < 6; i++) {
        EXPECT_TRUE(tree1.search(i));
    }
}


TEST_F(TreeSetTests, SearchValueTrue) {
    //given
    //when
    //then
    for (int i = 1; i < 6; i++) {
        EXPECT_TRUE(treeSet.search(i));
    }
}


TEST_F(TreeSetTests, SearchValueFalse) {
    //given
    //when
    //then
    EXPECT_FALSE(treeSet.search(11));
    EXPECT_FALSE(treeSet.search(123));
}


TEST_F(TreeSetTests, InsertNewValue) {
    //give
    //when
    treeSet.insert(6);
    //then
    EXPECT_TRUE(treeSet.search(6));
}


TEST_F(TreeSetTests, InsertDuplicateValue) {
    //give
    //when
    EXPECT_ANY_THROW(treeSet.insert(1));
}


TEST_F(TreeSetTests, DeleteValue) {
    //given
    //when
    treeSet.del(5);
    ASSERT_ANY_THROW(treeSet.del(123));
    //then
    EXPECT_FALSE(treeSet.search(5));
}


TEST_F(TreeSetTests, DeleteAllValues) {
    //given
    int b[3] = {1, 2};
    TreeSet<int> treeSet1(b, 2);
    //when
    treeSet1.del(1);
    treeSet1.del(2);
    //then
    EXPECT_FALSE(treeSet1.search(1));
    EXPECT_FALSE(treeSet1.search(2));
}


int sum1(int a, int b) {
    return a+b;
}


TEST_F(TreeSetTests, Reduce) {
    //given
    //when
    int res = treeSet.reduce(&sum1, 1);
    //then
    EXPECT_EQ(res, 16);
}


int square1(int a) {
    return a*a;
}


TEST_F(TreeSetTests, Map) {
    //given
    //when
    treeSet.map(&square1);
    //then
    for(int i = 1; i < 6; i++) {
        EXPECT_TRUE(treeSet.search(i*i));
    }
}


TEST_F(TreeSetTests, EqualSets) {
    //given
    int b[5] = {4, 1, 3, 2, 5};
    TreeSet<int> treeSet1(b, 5);
    TreeSet<int> treeSet2(b, 4);
    //when
    //then
    EXPECT_FALSE(treeSet.isEqualTo(treeSet2));
    EXPECT_TRUE(treeSet.isEqualTo(treeSet1));
}


TEST_F(TreeSetTests, SubSetFunction) {
    //given
    int b[3] = {5, 1, 3};
    TreeSet<int> treeSet1(b, 3);
    //when
    //then
    EXPECT_FALSE(treeSet1.contains(treeSet));
    EXPECT_TRUE(treeSet.contains(treeSet1));
}


TEST_F(TreeSetTests, SubSetFunctionEmpty) {
    //given
    TreeSet<int> treeSet1;
    //when
    //then
    EXPECT_TRUE(treeSet.contains(treeSet1));
}


