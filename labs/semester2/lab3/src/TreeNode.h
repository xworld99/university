#ifndef SRC_TREENODE_H
#define SRC_TREENODE_H

#include </home/egor/CLionProjects/LAB2_MAIN/src/CustomException.h>


template <typename T>
struct Node {
    T data;
    unsigned int height;
    Node *left;
    Node *right;
    Node(T _data) {
        data = _data;
        left = right = nullptr;
        height = 1;
    }
};


template <typename T>
unsigned int height(Node<T> *parent) {
    return parent ? parent->height : 0;
}


template <typename T>
int balanceFactor(Node<T> *parent) {
    return height(parent->right) - height(parent->left);
}


template <typename T>
void correctHeight(Node<T> *parent) {
    unsigned int h1 = height(parent->left);
    unsigned int h2 = height(parent->right);
    parent->height = (h1 > h2 ? h1 : h2) + 1;
}

template <typename T>
Node<T> *rotateRight(Node<T> *parent) {
    Node<T> *tmp = parent->left;
    parent->left = tmp->right;
    tmp->right = parent;
    correctHeight(parent);
    correctHeight(tmp);
    return tmp;
}


template <typename T>
Node<T> *rotateLeft(Node<T> *parent) {
    Node<T> *tmp = parent->right;
    parent->right = tmp->left;
    tmp->left = parent;
    correctHeight(parent);
    correctHeight(tmp);
    return tmp;
}


template <typename T>
Node<T> *balance(Node<T> *parent) {
    correctHeight(parent);
    if (balanceFactor(parent) == 2) {
        if (balanceFactor(parent->right) < 0) {
            parent->right = rotateRight(parent->right);
        }
        return rotateLeft(parent);
    }
    if (balanceFactor(parent) == -2) {
        if (balanceFactor(parent) > 0) {
            parent->left = rotateLeft(parent->left);
        }
        return rotateRight(parent);
    }
    return parent;
}


template <typename T>
Node<T> *findMin(Node<T> *parent) {
    return parent->left ? findMin(parent->left) : parent;
}

template <typename T>
Node<T> *removeMin(Node<T> *parent) {
    if (! parent->left) {
        return parent->right;
    }
    parent->left = removeMin(parent->left);
    return balance(parent);
}

template <typename T>
Node<T> *remove(Node<T> *parent, T value) {
    if (!parent) {
        throw DescriptiveException("Your list does not contains this value");
    }
    if (value < parent->data) {
        parent->left = remove(parent->left, value);
    } else if (parent->data < value) {
        parent->right = remove(parent->right, value);
    } else {
        Node<T> *left = parent->left;
        Node<T> *right = parent->right;
        delete parent;
        if (! right) {
            return left;
        }
        Node<T> *minR = findMin(right);
        minR->right = removeMin(right);
        minR->left = left;
        return balance(minR);
    }
    return balance(parent);
}


template <typename T>
Node<T> *copy(Node<T> *parent) {
    if (!parent) {
        return NULL;
    }
    Node<T> *newnode = new Node<T>(parent->data);;
    newnode->left = copy(parent->left);
    newnode->right = copy(parent->right);
    return newnode;
}


template <typename T>
Node<T> *deleteNodes(Node<T> *parent) {
    if (parent != nullptr) {
        deleteNodes(parent->left);
        deleteNodes(parent->right);
        delete parent;
    }
    return nullptr;
}



#endif