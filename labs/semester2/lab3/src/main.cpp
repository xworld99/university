#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>

#include "AVLTree.h"
#include </home/egor/CLionProjects/LAB2_MAIN/src/DynamicArray.h>


int abc(int b) {
    return b*b;
}


int acb(int frst, int scnd) {
    return frst+scnd;
}


bool searchValue(AVLTree<int> tree) {
    std::cout << "Enter a value to search in the tree: ";
    int value;
    std::cin >> value;
    return tree.search(value);
}


void reduceTree(AVLTree<int> tree) {
    std::cout << "Enter a value to start reduce from: ";
    int value;
    std::cin >> value;
    int res = tree.reduce(&acb, value);
    std::cout << "Reduce value : " << res << std::endl;
}


int addValue() {
    std::cout << "Enter new value: " << std::endl;
    int value;
    std::cin >> value;
    return value;
}


void format(AVLTree<int> tree) {
    std::cout << "Enter a string of formatting, p - parent, l - left, r - right" << std::endl;
    std::cout << "Tree will be output in the format P{L}[R]" << std::endl;
    std::string c;
    std::cin >> c;
    tree.print(c);
}


void getSubTreeA(AVLTree<int> mainTree) {
    std::cout << "Enter a value, which will be the head of a new tree: ";
    int value;
    std::cin >> value;
    AVLTree<int> tree = mainTree.getSubTree(value);
    std::cout << "This is sub tree: " << std::endl;
    tree.print("LPR");
}


void isSubTreeTest(AVLTree<int> mainTree) {
    AVLTree<int> tree;
    int len;
    std::cout << "Enter number of elements in new tree: ";
    std::cin >> len;
    std::cout << "Enter values of tree, that you need to check: " << std::endl;
    for (int i = 0; i < len; i++) {
        int a;
        std::cin >> a;
        tree.add(a);
    }
    std::cout << "This is new tree: " << std::endl;
    tree.print("LPR");
    if (mainTree.isSubTree(tree)) {
        std::cout << "Yes, this is sub tree!" << std::endl;
    } else {
        std::cout << "No, this isn`t sub tree =(" << std::endl;
    }
}


void printCommandText() {
    std::cout << "You can choose some commands from list below:" << std::endl;
    std::cout << "e (exit) - close application" << std::endl;
    std::cout << "s (search) - search for the value in your tree" << std::endl;
    std::cout << "m (map) - function that sqaures all the elements" << std::endl;
    std::cout << "r (reduce) - sums all tree elements starting from the entered value" << std::endl;
    std::cout << "a (add) - add new value" << std::endl;
    std::cout << "d (delete) - delete element with entered value" << std::endl;
    std::cout << "g (get sub tree) - returns sub tree of your main tree" << std::endl;
    std::cout << "p (print) - print sorted elements of your tree" << std::endl;
    std::cout << "f (format) - print elements of your tree in entered sequence" << std::endl;
    std::cout << "i (is sub tree) - check whether one tree is part of main" << std::endl;
    std::cout << "y - save elements of your main tree in Dynamic Array" << std::endl;
    std::cout << "t (tests) - run tests for big number of values" << std::endl;
}


int getLen() {
    int len;
    while (true) {
        std::cout << "Enter number of elements in tree: ";
        std::cin >> len;
        if (len >= 0) {
            break;
        } else {
            std::cout << "Number of elements should be positive" << std::endl;
        }
    }
    return len;
}


AVLTree<int> fillTree(int len) {
    srand(time(0));

    AVLTree<int> result;
    char c;
    std::cout << "Do you want to feel tree with random numbers? (y/n): ";
    while (true) {
        std::cin >> c;
        if (c == 'y') {
            int a;
            for (int i = 0; i < len; i++) {
                a = rand() % 13;
                result.add(a);
            }
            break;
        } else if (c == 'n') {
            std::cout << "Enter elements of the tree:" << std::endl;
            int a;
            for (int i = 0; i < len; i++) {
                std::cin >> a;
                result.add(a);
            }
            break;
        } else {
            std::cout << "Please enter correct value (y/n)" << std::endl;
        }
    }
    return result;
}


void testSearch() {
    srand(time(0));

    std::cout << "Time of search some value..." << std::endl;
    for (int i = 1; i <= 1000; i++) {
        std::cout << i*1000 << " elements in tree..." << std::endl;
        AVLTree<int> testTree;
        DynamicArray<int> valuesForSearch;
        for (int j = i*1000; j > 0; j--) {
            testTree.add(j);
        }

        unsigned int startTime = clock();
        for (int j = 1; j < 101; j++) {
            testTree.search(i*10*j);
        }
        unsigned int endTime = clock();
        unsigned int sTime = (endTime - startTime);

        std::ofstream out1;
        out1.open("/home/egor/CLionProjects/lab3/src/TimeTestsSearch", std::ios::app);
        if (out1.is_open()) {
            out1 << sTime << '\n';
        }
        out1.close();

        std::cout << "Success!" << std::endl;

        valuesForSearch.clear();
        testTree.clear();
    }
}


void testAdd() {
    srand(time(0));

    std::cout << "Time of add some value..." << std::endl;
    for (int i = 1; i <= 1000; i++) {
        std::cout << i*1000 << " elements in tree..." << std::endl;
        AVLTree<int> testTree;
        DynamicArray<int> valuesForSearch;
        for (int j = i*1000; j > 0; j--) {
            testTree.add(j);
        }

        unsigned int startTime = clock();
        for (int j = i*1000+1; j < i*1000 + 101; j++) {
            testTree.add(j);
        }
        unsigned int endTime = clock();
        unsigned int sTime = (endTime - startTime);

        std::ofstream out1;
        out1.open("/home/egor/CLionProjects/lab3/src/TimeTestsAdd", std::ios::app);
        if (out1.is_open()) {
            out1 << sTime << '\n';
        }
        out1.close();

        std::cout << "Success!" << std::endl;

        valuesForSearch.clear();
        testTree.clear();
    }
}


void runTests() {
    testSearch();
    testAdd();
}


int main() {
    int len = getLen();
    AVLTree<int> mainTree  = fillTree(len);

    printCommandText();

    char c = '!';
    while(c != 'e') {
        std::cout << "Choose a command from list: ";
        std::cin >> c;

        switch(c) {
            case 's':
                if (searchValue(mainTree)) {
                    std::cout << "This value has been found!" << std::endl;
                } else {
                    std::cout << "This value isn`t in your tree =(" << std::endl;
                }
                break;

            case 'm':
                mainTree.map(&abc);
                std::cout << "Success!" << std::endl;
                break;

            case 'r':
                reduceTree(mainTree);
                break;

            case 'a':
                mainTree.add(addValue());
                std::cout << "Success!" << std::endl;
                break;

            case 'd':
                std::cout << "Enter value to delete: ";
                int value;
                std::cin >> value;
                mainTree.del(value);
                std::cout << "Success!" << std::endl;
                break;

            case 'g':
                getSubTreeA(mainTree);
                break;

            case 'p':
                mainTree.printSort();
                break;

            case 'f':
                format(mainTree);
                break;

            case 'i':
                isSubTreeTest(mainTree);
                break;

            case 'y': {
                DynamicArray<int> resArr = mainTree.saveToArray();
                std::cout << "Array contains next numbers: " << std::endl;
                resArr.print();
                break;
            }

            case 't' :
                runTests();

            default:
                std::cout << "Please choose right command from list" << std::endl;
                break;
        }
    }
    return 0;
}
