#ifndef UNTITLED_MATRIX_H
#define UNTITLED_MATRIX_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define INT_TYPE 1
#define COMPLEX_TYPE 2


struct matrix{
    struct element **buffer;
    int dimension;
};


struct element{
    void *value;
    int type;
};


struct complex{
    float real;
    float imaginary;
};


struct matrix* create(int dimension);
struct matrix* sum(struct matrix *mat1, struct matrix *mat2);
struct matrix* multiply(struct matrix *mat1, struct matrix *mat2);
struct matrix* copy(struct matrix *mat);
void multiply_by_number(struct element num, struct matrix *mat);


struct element add_elements(struct element el1, struct element el2);
struct element add_int(struct element el1, struct element el2);
struct element add_complex(struct element el1, struct element el2);
struct element add_int_to_complex(struct element el1, struct element el2);
struct element multiply_elements(struct element el1, struct element el2);
struct element multiply_int(struct element el1, struct element el2);
struct element multiply_complex(struct element el1, struct element el2);
struct element multiply_int_and_complex(struct element el1, struct element el2);
struct element get_aux_el();
int is_equals(struct element el1, struct element el2);


void set_default(struct matrix *matrix);
void set_element(struct matrix *mat, int col, int row, struct element value);
void destroy_element(struct element el);
void fill(struct matrix *mat);
void destroy(struct matrix *mat);
void get_matrix(struct matrix *mat);
void print_int(int value);
void print_complex(struct complex comp);


struct matrix* create(int dimension){
    struct matrix *mat = malloc(sizeof(struct matrix));     /* выделяем память для указателя на матрицу */
    mat->buffer = malloc(dimension * sizeof(void*));   /* выделяем память для матрицы */
    for (int i = 0; i < dimension; ++i){
        mat->buffer[i] = (malloc(dimension * sizeof(struct element)));
        for (int k = 0; k < dimension; ++k){
            mat->buffer[i][k].value = NULL;
        }
    }
    mat->dimension = dimension;
    set_default(mat);
    return mat;
}


struct matrix* copy(struct matrix *mat){
    int dim = mat->dimension;
    struct matrix *res = create(dim);
    for (int i = 0; i < dim; ++i){
        for (int j = 0; j < dim; ++j){
            set_element(res, i, j, mat->buffer[i][j]);
        }
    }
    return res;
}


void destroy(struct matrix *mat){
    if (mat != NULL){
        int dim = mat->dimension;
        for (int i = 0; i < dim; ++i){
            for (int j = 0; j < dim; ++j){
                destroy_element(mat->buffer[i][j]);
            }
            free(mat->buffer[i]);
        }
        free(mat->buffer);
        free(mat);
    }
}


void destroy_element(struct element el){
    free(el.value);
}


struct matrix* sum(struct matrix *mat1, struct matrix *mat2){
    int dim1 = mat1->dimension;
    int dim2 = mat2->dimension;
    if (dim1 != dim2){
        return NULL;
    }
    struct matrix *res_mat = create(dim1);
    struct element el1;
    struct element el2;
    struct element res_el;
    for (int i = 0; i < dim1; ++i){
        for (int j = 0; j < dim1; ++j){
            el1 = mat1->buffer[i][j];
            el2 = mat2->buffer[i][j];
            res_el = add_elements(el1, el2);
            set_element(res_mat, i, j, res_el);
            destroy_element(res_el);
        }
    }
    return res_mat;
}


struct matrix* multiply(struct matrix *mat1, struct matrix *mat2){
    int dim1 = mat1->dimension;
    int dim2 = mat2->dimension;
    if (dim1 != dim2){
        return NULL;
    }
    struct matrix *res_mat = create(dim1);
    struct element el1;
    struct element el2;
    struct element res_el;
    struct element tmp1, tmp2;
    int size;
    for (int i = 0; i < dim1; ++i){
        for (int j = 0; j < dim1; ++j){
            res_el.value = malloc(sizeof(int));
            *((int *) res_el.value) = 0;
            res_el.type = 1;
            for (int k = 0; k < dim1; ++k){
                el1 = mat1->buffer[i][k];
                el2 = mat2->buffer[k][j];
                tmp1 = multiply_elements(el1, el2);
                size = (res_el.type == INT_TYPE) ? sizeof(int) : sizeof(struct complex);
                tmp2.value = malloc(size);
                tmp2.type = res_el.type;
                memcpy(tmp2.value, res_el.value, size);
                res_el = add_elements(tmp2, tmp1);
                destroy_element(tmp1);
                destroy_element(tmp2);
            }
            set_element(res_mat, i, j, res_el);
            destroy_element(res_el);
        }
    }
    return res_mat;
}


void multiply_by_number(struct element num, struct matrix *mat){
    int dim = mat->dimension;
    struct element res_el;
    for (int i = 0; i < dim; ++i){
        for(int j = 0; j < dim; ++j){
            res_el = multiply_elements(num, mat->buffer[i][j]);
            set_element(mat, i, j, res_el);
            destroy_element(res_el);
        }
    }
}


void set_default(struct matrix *mat){
    struct element el;
    int dim = mat->dimension;
    el.value = malloc(sizeof(int));
    *((int *) el.value) = 0;
    el.type = INT_TYPE;
    for (int i = 0; i < dim; ++i){
        for (int j = 0; j < dim; ++j){
            set_element(mat, i, j, el);
        }
    }
    destroy_element(el);
}


void set_element(struct matrix *mat, int col, int row, struct element value){
    int size;
    struct complex comp;
    if (value.type == COMPLEX_TYPE){
        comp.real = ((struct complex *) value.value)->real;
        comp.imaginary = ((struct complex *) value.value)->imaginary;
        if ((comp.real == 0) && (comp.imaginary == 0)){
            value.type = INT_TYPE;
            *((int *) value.value) = 0;
        }
    }
    destroy_element(mat->buffer[col][row]);
    size = (value.type == INT_TYPE) ? sizeof(int) : sizeof(struct complex);
    mat->buffer[col][row].value = malloc(size);
    mat->buffer[col][row].type = value.type;
    memcpy(mat->buffer[col][row].value, value.value, size);
}


void get_matrix(struct matrix *mat){
    if (mat != NULL){
        int dim = mat->dimension;
        struct element el;
        for (int i = 0; i < dim; ++i){
            for (int j = 0; j < dim; ++j){
                el = mat->buffer[i][j];
                if (el.type == INT_TYPE){
                    print_int(*(int *) el.value);
                } else {
                    print_complex(*(struct complex *) (el.value));
                }
            }
            printf("\n");
        }
    } else{
        printf("This matrix is empty =(");
    }
}


void fill(struct matrix *mat){
    int dim = mat->dimension;
    struct element tmp_el;
    for (int i = 0; i < dim; ++i){
        for (int j = 0; j < dim; ++j){
            tmp_el = get_aux_el();
            set_element(mat, i, j, tmp_el);
            destroy_element(tmp_el);
        }
    }
}


struct element get_aux_el(){
    struct element el;
    struct complex comp;
    int f = ((int) random()) % 2;
    if (!f){
        el.type = INT_TYPE;
        el.value = malloc(sizeof(int));
        *((int *) el.value) = ((int) random()) % 10;
    } else{
        el.type = COMPLEX_TYPE;
        el.value = malloc(sizeof(struct complex));
        comp.imaginary = ((int) random()) % 10;
        comp.real = ((int) random()) % 10;
        *((struct complex *) el.value) = comp;
    }
    return el;
}


struct element add_elements(struct element el1, struct element el2){
    struct element res_el;
    if (el1.type == INT_TYPE){
        if (el2.type == INT_TYPE){
            res_el = add_int(el1, el2);
        } else{
            res_el = add_int_to_complex(el1, el2);
        }
    } else{
        if (el2.type == INT_TYPE){
            res_el = add_int_to_complex(el2, el1);
        } else{
            res_el = add_complex(el1, el2);
        }
    }
    return res_el;
}


struct element add_int(struct element el1, struct element el2){
    struct element res_el;
    int sum;
    res_el.type = INT_TYPE;
    res_el.value = malloc(sizeof(int));
    sum = *((int *) el1.value) + *((int *) el2.value);
    *((int *) res_el.value) = sum;
    return res_el;
}


struct element add_int_to_complex(struct element el1, struct element el2) {
    struct element res_el;
    struct complex res_comp;
    res_el.type = COMPLEX_TYPE;
    res_el.value = malloc(sizeof(struct complex));
    res_comp.real = (float) (*(int *) el1.value) + (*(struct complex *) (el2.value)).real;
    res_comp.imaginary = (*(struct complex *) (el2.value)).imaginary;
    *((struct complex *) res_el.value) = res_comp;
    return res_el;
}


struct element add_complex(struct element el1, struct element el2) {
    struct element res_el;
    struct complex res_comp;
    res_el.type = COMPLEX_TYPE;
    res_el.value = malloc(sizeof(struct complex));
    res_comp.real = (*(struct complex *) (el1.value)).real + (*(struct complex *) (el2.value)).real;
    res_comp.imaginary = (*(struct complex *) (el1.value)).imaginary + (*(struct complex *) (el2.value)).imaginary;
    *((struct complex *) res_el.value) = res_comp;
    return res_el;
}


struct element multiply_elements(struct element el1, struct element el2){
    struct element res_el;
    if (el1.type == INT_TYPE){
        if (el2.type == INT_TYPE){
            res_el = multiply_int(el1, el2);
        } else{
            res_el = multiply_int_and_complex(el1, el2);
        }
    } else{
        if (el2.type == INT_TYPE){
            res_el = multiply_int_and_complex(el2, el1);
        } else{
            res_el = multiply_complex(el1, el2);
        }
    }
    return res_el;
}


struct element multiply_int(struct element el1, struct element el2){
    struct element res_el;
    int mult;
    res_el.type = INT_TYPE;
    res_el.value = malloc(sizeof(int));
    mult = *((int *) el1.value) * *((int *) el2.value);
    *((int *) res_el.value) = mult;
    return res_el;
}


struct element multiply_complex(struct element el1, struct element el2){
    struct element res_el;
    struct complex res_comp;
    res_el.type = COMPLEX_TYPE;
    res_el.value = malloc(sizeof(struct complex));
    res_comp.real = (*(struct complex *) (el1.value)).real * (*(struct complex *) (el2.value)).real
                     - (*(struct complex *) (el1.value)).imaginary * (*(struct complex *) (el2.value)).imaginary;
    res_comp.imaginary = (*(struct complex*) (el1.value)).real * (*(struct complex*) (el2.value)).imaginary
                         + (*(struct complex*) (el1.value)).imaginary * (*(struct complex*) (el2.value)).real;
    *((struct complex*) res_el.value) = res_comp;
    return res_el;
}


struct element multiply_int_and_complex(struct element el1, struct element el2){
    struct element res_el;
    struct complex res_comp;
    if(*((int *) el1.value ) != 0) {
        res_el.type = COMPLEX_TYPE;
        res_el.value = malloc(sizeof(struct complex));
        res_comp.real = (float) (*(int *) el1.value) * (*(struct complex *) (el2.value)).real;
        res_comp.imaginary = (float) (*(int *) el1.value) * (*(struct complex *) (el2.value)).imaginary;
        *((struct complex *) res_el.value) = res_comp;
    }
    else{
       res_el.type = INT_TYPE;
        res_el.value = malloc(sizeof(int));
        *((int *) res_el.value) = 0;
    }
    return res_el;
}


int is_equals(struct element el1, struct element el2){
    int equals = 0;
    struct complex comp1, comp2;
    if ((el1.type == INT_TYPE) && (el2.type == INT_TYPE)){
        if (*((int *) el1.value) == *((int *) el2.value)){
            equals = 1;
        }
    }
    if ((el1.type == COMPLEX_TYPE) && (el2.type == COMPLEX_TYPE)){
        comp1.imaginary = ((struct complex *) el1.value)->imaginary;
        comp1.real = ((struct complex *) el1.value)->real;
        comp2.imaginary = ((struct complex *) el2.value)->imaginary;
        comp2.real = ((struct complex *) el2.value)->real;
        if ((comp1.imaginary == comp2.imaginary) && (comp1.real == comp2.real)){
            equals = 1;
        }
    }
    return equals;
}


void print_int(int value){
    printf("%d ", value);
}


void print_complex(struct complex comp){
    if (comp.imaginary) {
        printf("%.1f+%.1fi ", comp.real, comp.imaginary);
    } else {
        printf("%.1f ", comp.real);
    }
}


#endif