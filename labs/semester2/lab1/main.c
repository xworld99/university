#include <stdio.h>
#include <time.h>
#include "matrix.h"


#define NUMBER_OF_TESTS 10


void test_add_matrix();
void test_multiply_by_number();
void test_multiply_matrix();


int main() {
    test_add_matrix();

    test_multiply_by_number();

    test_multiply_matrix();

    return 0;
}


void test_add_matrix(){
    int n;
    int error;
    struct matrix *matrix1;
    struct matrix *matrix2;
    struct matrix *res1;
    struct element aux;
    struct element res_el;


    for (int k = 0; k < NUMBER_OF_TESTS; ++k) {
        n = time(NULL) * random()%10 + 1;
        matrix1 = create(n);
        matrix2 = create(n);
        fill(matrix1);
        fill(matrix2);
        printf("sum matrix with dimensions %d\n", n);
        error = 0;
        res1 = sum(matrix1, matrix2);
        for (int i = 0; i < n; ++i){
            for (int j = 0; j < n; ++j){
                aux = add_elements(matrix1->buffer[i][j], matrix2->buffer[i][j]);
                res_el = res1->buffer[i][j];
                if (! is_equals(aux, res_el)){
                    error = 1;
                    break;
                }
                destroy_element(aux);
            }
        }
        if (!error) {
            printf("\tworking!\n");
        } else {
            printf("\tah, shit...\n");
        }
        destroy(res1);
        destroy(matrix1);
        destroy(matrix2);
    }

    printf("try to sum matrix with different dimensions\n");
    n = time(NULL) * random() % 10 + 1;
    matrix1 = create(n);
    matrix2 = create(n + 3);
    fill(matrix1);
    fill(matrix2);
    res1 = sum(matrix1, matrix2);
    if (res1 == NULL) {
        printf("\treturns NULL, working!\n");
    } else {
        printf("\tah, shit...\n");
    }
    destroy(res1);
    destroy(matrix1);
    destroy(matrix2);

    n = time(NULL) * random() % 10 + 1;
    printf("sum opposite matrixes with dimensions %d\n", n);
    error = 0;
    matrix1 = create(n);
    fill(matrix1);
    matrix2 = copy(matrix1);
    aux.type = INT_TYPE;
    aux.value = malloc(sizeof(int));
    *((int *) aux.value) = -1;
    multiply_by_number(aux, matrix1);
    res1 = sum(matrix1, matrix2);
    destroy(matrix1);
    destroy(matrix2);
    matrix1 = create(n);
    for (int i = 0; i < n; ++i){
        for (int j = 0; j < n; ++j){
            if (! is_equals(matrix1->buffer[i][j], res1->buffer[i][j])){
                error = 1;
                break;
            }
        }
    }

    if (! error){
        printf("\tworking!\n");
    } else{
        printf("\tah, shit\n");
    }
    printf("\n");
}

void test_multiply_by_number(){
    int n;
    int r;
    int error = 0;
    struct element el;
    struct element aux;
    struct matrix *first;
    struct matrix *matrix;

    for (int k = 0; k < NUMBER_OF_TESTS; ++k) {
        n = time(NULL)*random()%10 + 1;
        r = time(NULL)*random()%10;
        printf("multiply matrix with dimension %d by int number %d\n", n, r);
        error = 0;
        matrix = create(n);
        fill(matrix);
        first = copy(matrix);
        el.type = INT_TYPE;
        el.value = malloc(sizeof(int));
        *((int *) el.value) = r;
        multiply_by_number(el, matrix);
        for (int i = 0; i < n; ++i){
            for (int j = 0; j < n; ++j){
                aux = multiply_elements(el, first->buffer[i][j]);
                if (! is_equals(aux, matrix->buffer[i][j])){
                    error = 1;
                    break;
                }
                destroy_element(aux);
            }
        }
        if (!error){
            printf("\tworking!\n");
        } else{
            printf("\tah, shit...\n");
        }
        destroy_element(el);
        destroy(first);
        destroy(matrix);
    }


    for (int k = 0; k < NUMBER_OF_TESTS; k++){
        n = time(NULL) * random() % 10 + 1;
        r = time(NULL) * random() % 10;
        struct complex comp;
        comp.real = (float) (r + 3);
        comp.imaginary = (float) ((r + 13)) / 4;
        printf("multiply matrix with dimension %d by complex number ", n);
        print_complex(comp);
        printf("\n");
        error = 0;
        matrix = create(n);
        fill(matrix);
        first = copy(matrix);
        el.type = COMPLEX_TYPE;
        el.value = malloc(sizeof(struct complex));
        *((struct complex *) el.value) = comp;
        multiply_by_number(el, matrix);
        for (int i = 0; i < n; ++i){
            for (int j = 0; j < n; ++j){
                aux = multiply_elements(first->buffer[i][j], el);
                if (! is_equals(aux, matrix->buffer[i][j])) {
                    error = 1;
                    break;
                }
            }
        }
        if (!error) {
            printf("\tworking!\n");
        } else {
            printf("\tah, shit...\n");
        }
        destroy(matrix);
        destroy(first);
    }
    printf("\n");
}


void test_multiply_matrix(){
    int n;
    int size;
    int error = 0;
    struct element el, el1, el2, tmp1, tmp2;
    struct matrix *matrix1;
    struct matrix *matrix2;
    struct matrix *res;

    for (int m = 0; m < NUMBER_OF_TESTS; ++m){
        n = time(NULL) * random() % 10 + 1;
        printf("multiply matrix with dimension %d\n", n);
        matrix1 = create(n);
        matrix2 = create(n);
        fill(matrix1);
        fill(matrix2);
        res = multiply(matrix1, matrix2);
        for (int i = 0; i < n; ++i){
            for (int j = 0; j < n; ++j){
                el.value = malloc(sizeof(int));
                *((int*)el.value) = 0;
                el.type = 1;
                for (int k = 0; k < n; ++k){
                    el1 = matrix1->buffer[i][k];
                    el2 = matrix2->buffer[k][j];
                    tmp1 = multiply_elements(el1, el2);
                    size = (el.type == INT_TYPE) ? sizeof(int) : sizeof(struct complex);
                    tmp2.value = malloc(size);
                    tmp2.type = el.type;
                    memcpy(tmp2.value, el.value, size);
                    el = add_elements(tmp2, tmp1);
                    destroy_element(tmp1);
                    destroy_element(tmp2);
                }
                if(! is_equals(el, res->buffer[i][j])){
                    error = 1;
                    break;
                }
                destroy_element(el);
            }
        }
        if (!error){
            printf("\tworking!\n");
        } else{
            printf("\tah, shit...\n");
        }
    }

    printf("try to multiply matrixes with different dimensions");
    printf("try to sum matrix with different dimensions\n");
    n = time(NULL) * random() % 10 + 1;
    matrix1 = create(n);
    matrix2 = create(n+3);
    fill(matrix1);
    fill(matrix2);
    res = sum(matrix1, matrix2);
    if (res == NULL){
        printf("\treturns NULL, working!\n");
    } else{
        printf("\tah, shit...\n");
    }
    destroy(res);
    destroy(matrix1);
    destroy(matrix2);
}