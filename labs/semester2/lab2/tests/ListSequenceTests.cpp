#include </home/egor/CLionProjects/LAB2_MAIN/src/ListSequence.h>
#include </home/egor/CLionProjects/LAB2_MAIN/src/Sequence.h>
#include <gtest/gtest.h>


class ListSequenceTest : public ::testing::Test {
protected:
    Sequence<int> *data;
    void SetUp() override {
        int a[5] = {1, 2, 3, 4, 5};
        data = new ListSequence<int>(a, 5);
    }
    void TearDown() override {
        delete data;
    }
};


TEST_F(ListSequenceTest, CreateEmpty) {
    //given
    Sequence<int> *da = new ListSequence<int>();
    //when
    //then
    EXPECT_EQ(0, da->getLength());
}


TEST_F(ListSequenceTest, CreateFromArray) {
    //given
    //when
    //then
    EXPECT_EQ(5, data->getLength());
    for (int i = 0; i < data->getLength(); i++) {
        EXPECT_EQ(i+1, (*data)[i]);
    }
}


TEST_F(ListSequenceTest, CreateFromAnotherSequence) {
    //given
    int a[5] = {1, 2, 3, 4, 5};
    ListSequence<int> da(a, 5);
    ListSequence<int> b = da;
    //when
    //then
    EXPECT_EQ(da.getLength(), b.getLength());
    for(int i = 0; i < da.getLength(); i++) {
        EXPECT_EQ(da[i], b[i]);
    }
}


TEST_F(ListSequenceTest, GetFunction) {
    //given
    //when
    //then
    EXPECT_EQ(1, (*data)[0]);
    EXPECT_EQ(3, (*data)[2]);
    EXPECT_THROW((*data)[12], DescriptiveException);
    EXPECT_THROW((*data)[-12], DescriptiveException);
}


TEST_F(ListSequenceTest, SetFunction) {
    //given
    //when
    data->set(1, 15);
    EXPECT_EQ(5, data->getLength());
    EXPECT_EQ(15, (*data)[1]);
    EXPECT_THROW(data->set(13, 0), DescriptiveException);
    EXPECT_THROW(data->set(-13, 0), DescriptiveException);
}


TEST_F(ListSequenceTest, AppendFunction) {
    //given
    //when
    data->append(6);
    //then
    EXPECT_EQ(6, data->getLength());
    EXPECT_EQ(6, (*data)[5]);
}


TEST_F(ListSequenceTest, InsertFunction) {
    //given
    //when
    data->insert(2, 13);
    //then
    EXPECT_EQ(6, data->getLength());
    EXPECT_EQ(13, (*data)[2]);
    EXPECT_THROW(data->insert(-12, 13), DescriptiveException);
    EXPECT_THROW(data->insert(12, 13), DescriptiveException);
}


TEST_F(ListSequenceTest, RemoveFunctionFromEnd) {
    //given
    //when
    data->remove(5);
    //then
    EXPECT_EQ(4, data->getLength());
    for (int i = 0; i < data->getLength(); i++) {
        EXPECT_NE(5, (*data)[i]);
    }
}


TEST_F(ListSequenceTest, RemoveFunction) {
    //given
    //when
    data->remove(3);
    //then
    EXPECT_EQ(4, data->getLength());
    for (int i = 0; i < data->getLength(); i++) {
        EXPECT_NE(3, (*data)[i]);
    }
    EXPECT_THROW(data->remove(12), DescriptiveException);
    EXPECT_THROW(data->remove(-12), DescriptiveException);
}


TEST_F(ListSequenceTest, RemoveFunctionWithEqualValues) {
    //given
    int a[3] = {1, 2, 1};
    ListSequence<int> da(a, 3);
    //when
    da.remove(1);
    //then
    EXPECT_EQ(2, da.getLength());
    EXPECT_EQ(2, da[0]);
    EXPECT_EQ(1, da[1]);
}


TEST_F(ListSequenceTest, RemoveAllValueFromSequence) {
    //given
    int a[2] = {1, 2};
    ListSequence<int> da(a, 2);
    //when
    da.remove(1);
    da.remove(2);
    //then
    EXPECT_EQ(0, da.getLength());
    EXPECT_THROW(da[2], DescriptiveException);
}



TEST_F(ListSequenceTest, PopFunctionWithoutArgs) {
    //given
    //when
    int a = data->pop();
    //then
    EXPECT_EQ(a, 5);
    EXPECT_EQ(4, data->getLength());
    for (int i = 0; i < data->getLength(); i++) {
        EXPECT_EQ(i+1, (*data)[i]);
    }
}


TEST_F(ListSequenceTest, PopFunction) {
    //given
    //when
    int a = data->pop(2);
    //then
    EXPECT_EQ(a, 3);
    EXPECT_EQ(4, data->getLength());
    for (int i = 0; i < data->getLength(); i++) {
        EXPECT_NE(a, (*data)[i]);
    }
    EXPECT_THROW(data->pop(12), DescriptiveException);
    EXPECT_THROW(data->pop(-12), DescriptiveException);
}


TEST_F(ListSequenceTest, PopFromEmtyArray) {
    //given
    Sequence<int> *a = new ListSequence<int>();
    //when
    //then
    EXPECT_THROW(a->pop(), DescriptiveException);
    EXPECT_THROW(a->pop(1), DescriptiveException);
}


TEST_F(ListSequenceTest, PopAllValues) {
    //given
    int a[1] = {1};
    Sequence<int> *da = new ListSequence<int>(a, 1);
    //when
    da->pop();
    //then
    EXPECT_EQ(0, da->getLength());
}


int plus3(int a){
    return a+3;
}


TEST_F(ListSequenceTest, MapFunction) {
    //given
    //when
    data->map(&plus3);
    //then
    EXPECT_EQ(5, data->getLength());
    for (int i = 0; i < data->getLength(); i++) {
        EXPECT_EQ(plus3(i+1), (*data)[i]);
    }
}