#include </home/egor/CLionProjects/LAB2_MAIN/src/DynamicArray.h>
#include </home/egor/CLionProjects/LAB2_MAIN/src/CustomException.h>
#include <gtest/gtest.h>


class DynamicArrayTest : public ::testing::Test {
protected:
    DynamicArray<int> data;
    void SetUp() override {
        int a[5] = {1, 2, 3, 4, 5};
        data = DynamicArray<int>(a, 5);
    }
    void TearDown() override {
        data.clear();
    }
};


TEST_F(DynamicArrayTest, CreateEmpty) {
    //given
    DynamicArray<int> a = DynamicArray<int>();
    //when
    //then
    EXPECT_EQ(0, a.getLength());
}




TEST_F(DynamicArrayTest, CreateFromArray) {
    //given
    //when
    //then
    EXPECT_EQ(5, data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_EQ(i+1, data[i]);
    }
}


TEST_F(DynamicArrayTest, CreateFromEmptyArray) {
    //given
    int a[0] = {};
    DynamicArray<int> b(a, 0);
    //when
    //then
    EXPECT_EQ(0, b.getLength());
}


TEST_F(DynamicArrayTest, CreateFromAnotherArray) {
    //given
    DynamicArray<int> a = data;
    //when
    //then
    EXPECT_EQ(a.getLength(), data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_EQ(i+1, data[i]);
    }
}


TEST_F(DynamicArrayTest, AssignmentOperator) {
    //given
    DynamicArray<int> a;
    //when
    a = data;
    //then
    EXPECT_EQ(a.getLength(), data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_EQ(a[i], data[i]);
    }
}


TEST_F(DynamicArrayTest, GetFunction) {
    //given
    //when
    //then
    EXPECT_EQ(1, data[0]);
    EXPECT_THROW(data[12], DescriptiveException);
    EXPECT_THROW(data[-12], DescriptiveException);
}


TEST_F(DynamicArrayTest, SetFunction) {
    //given
    //when
    data.set(3, 13);
    //then
    EXPECT_EQ(5, data.getLength());
    EXPECT_EQ(13, data[3]);
    EXPECT_THROW(data.set(13, 0), DescriptiveException);
    EXPECT_THROW(data.set(-13, 0), DescriptiveException);
}


TEST_F(DynamicArrayTest, AppendFunction) {
    //given
    //when
    data.append(6);
    //then
    EXPECT_EQ(6, data.getLength());
    EXPECT_EQ(6, data[5]);
}


TEST_F(DynamicArrayTest, TestCapacity) {
    int b[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    DynamicArray<int> a(b, 9);
    a.append(10);
    a.append(11);
    EXPECT_EQ(20, a.getCapacity());
    a.remove(11);
    EXPECT_EQ(10, a.getCapacity());
}


TEST_F(DynamicArrayTest, InsertFunction) {
    //given
    //when
    data.insert(2, 13);
    //then
    EXPECT_EQ(6, data.getLength());
    EXPECT_EQ(13, data[2]);
    EXPECT_THROW(data.insert(-12, 13), DescriptiveException);
    EXPECT_THROW(data.insert(12, 13), DescriptiveException);
}


TEST_F(DynamicArrayTest, RemoveFunctionFromEnd) {
    //given
    //when
    data.remove(5);
    //then
    EXPECT_EQ(4, data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_NE(5, data[i]);
    }
}


TEST_F(DynamicArrayTest, RemoveFunction) {
    //given
    //when
    data.remove(3);
    //then
    EXPECT_EQ(4, data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_NE(3, data[i]);
    }
    EXPECT_THROW(data.remove(12), DescriptiveException);
    EXPECT_THROW(data.remove(-12), DescriptiveException);
}


TEST_F(DynamicArrayTest, RemoveFunctionWithEqualValues) {
    //given
    int a[3] = {1, 2, 1};
    DynamicArray<int> da(a, 3);
    //when
    da.remove(1);
    //then
    EXPECT_EQ(2, da.getLength());
    EXPECT_EQ(2, da[0]);
    EXPECT_EQ(1, da[1]);
}


TEST_F(DynamicArrayTest, RemoveAllValueFromArray) {
    //given
    int a[2] = {1, 2};
    DynamicArray<int> da(a, 2);
    //when
    da.remove(1);
    da.remove(2);
    //then
    EXPECT_EQ(0, da.getLength());
}


TEST_F(DynamicArrayTest, RemoveFromEmptyArray) {
    //given
    DynamicArray<int> a;
    //when
    //then
    EXPECT_THROW(a.remove(12), DescriptiveException);
}


TEST_F(DynamicArrayTest, PopFunctionWithoutArgs) {
    //given
    //when
    int a = data.pop();
    //then
    EXPECT_EQ(a, 5);
    EXPECT_EQ(4, data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_EQ(i+1, data[i]);
    }
}


TEST_F(DynamicArrayTest, PopFunction) {
    //given
    //when
    int a = data.pop(2);
    //then
    EXPECT_EQ(a, 3);
    EXPECT_EQ(4, data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_NE(a, data[i]);
    }
    EXPECT_THROW(data.pop(12), DescriptiveException);
    EXPECT_THROW(data.pop(-12), DescriptiveException);
}


TEST_F(DynamicArrayTest, PopFromEmtyArray) {
    //given
    DynamicArray<int> a;
    //when
    //then
    EXPECT_THROW(a.pop(), DescriptiveException);
    EXPECT_THROW(a.pop(1), DescriptiveException);
}


TEST_F(DynamicArrayTest, PopAllValues) {
    //given
    int a[1] = {1};
    DynamicArray<int> da(a, 1);
    //when
    da.pop();
    //then
    EXPECT_EQ(0, da.getLength());
    EXPECT_EQ(0, da.getCapacity());
}


TEST_F(DynamicArrayTest, Concationation) {
    //given
    int b[3] = {12, 13, 14};
    DynamicArray<int> data2(b, 3);
    //when
    data.concat(data2);
    //then
    ASSERT_EQ(8, data.getLength());
    for (int i = 0; i < 5; i++) {
        EXPECT_EQ(i+1, data[i]);
    }
    EXPECT_EQ(14, data[7]);
    EXPECT_EQ(13, data[6]);
    EXPECT_EQ(12, data[5]);
}


int square(int a){
    return a*a;
}


TEST_F(DynamicArrayTest, MapFunction) {
    //given
    //when
    data.map(&square);
    //then
    EXPECT_EQ(5, data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_EQ(square(i+1), data[i]);
    }
}


bool removed(int a) {
    return a < 3;
}


TEST_F(DynamicArrayTest, RemoveIfInSorted) {
    //given
    //when
    data.removeIf(&removed);
    //then
    EXPECT_EQ(3, data.getLength());
    for (int i = 0; i < 3; i++) {
        EXPECT_FALSE(removed(data[i]));
    }
}


TEST_F(DynamicArrayTest, RemoveIfRandom) {
    //given
    int a[5] = {8, 2, 0, 7, 1};
    DynamicArray<int> da(a, 5);
    //when
    da.removeIf(&removed);
    //then
    ASSERT_EQ(2, da.getLength());
    for (int i = 0; i < 2; i++) {
        EXPECT_FALSE(removed(da[i]));
    }
}


TEST_F(DynamicArrayTest, RemoveIfNothing) {
    //given
    int a[1] = {123};
    DynamicArray<int> da(a, 1);
    //when
    da.removeIf(&removed);
    //then
    ASSERT_EQ(1, da.getLength());
    EXPECT_EQ(123, da[0]);
}


TEST_F(DynamicArrayTest, RemoveIfAll) {
    //given
    int a[2] = {1, 2};
    DynamicArray<int> da(a, 2);
    //when
    da.removeIf(&removed);
    //then
    EXPECT_EQ(0, da.getLength());
    EXPECT_EQ(10, da.getCapacity());
}