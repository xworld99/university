#ifndef LAB2_ARRAYSEQUENCE_H
#define LAB2_ARRAYSEQUENCE_H

#include <iostream>
#include "Sequence.h"


template<typename T>
class ArraySequence : public Sequence<T> {
public:
    ArraySequence();
    ArraySequence(T *data, int length);
    ArraySequence(const ArraySequence<T> &assignable);
    ArraySequence<T>& operator=(const ArraySequence<T> &assignable);
    virtual void map(T (*funcPtr)(T)) override;
    virtual void insert(int pos, T value) override;
    virtual void remove(T value) override;
    virtual T pop(int pos = -1) override;
    virtual void print() override;
    virtual void set(int pos, T value) override;
    virtual T operator[](int pos) override;
    virtual int getLength() override;
    virtual void append(T value) override;
    virtual ~ArraySequence();
private:
    DynamicArray<T> items;
};


template<typename T>
ArraySequence<T>::ArraySequence()
    : items() {
}


template<typename T>
ArraySequence<T>::ArraySequence(T *data, int length)
 : items(data, length) {
}


template<typename T>
ArraySequence<T>::ArraySequence(const ArraySequence<T> &assignable) {
    if (&assignable != this) {
        items = DynamicArray<T>(assignable.items);
    }
}


template<typename T>
ArraySequence<T> & ArraySequence<T>::operator=(const ArraySequence<T> &assignable) {
    if (&assignable != this) {
        items = assignable.items;
    }
}


template<typename T>
ArraySequence<T>::~ArraySequence(){
    items.clear();
}


template<typename T>
inline void ArraySequence<T>::map(T (*funcPtr)(T)) {
    items.map(funcPtr);
}


template<typename T>
inline void ArraySequence<T>::insert(int pos, T value) {
    items.insert(pos, value);
}


template<typename T>
inline T ArraySequence<T>::operator[](int pos) {
    return items[pos];
}


template<typename T>
inline void ArraySequence<T>::remove(T value) {
    items.remove(value);
}


template<typename T>
inline T ArraySequence<T>::pop(int pos /* = -1 */) {
    return items.pop(pos);
}


template<typename T>
inline void ArraySequence<T>::set(int pos, T value) {
    items.set(pos, value);
}


template<typename T>
inline void ArraySequence<T>::append(T value) {
    items.append(value);
}


template<typename T>
inline void ArraySequence<T>::print() {
    items.print();
}


template<typename T>
inline int ArraySequence<T>::getLength() {
    return items.getLength();
}


#endif
