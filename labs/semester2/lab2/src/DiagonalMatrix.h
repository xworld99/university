#ifndef LAB2_DIAGONALMATRIX_H
#define LAB2_DIAGONALMATRIX_H

#include <iostream>
#include <cmath>
#include "ArraySequence.h"
#include "ListSequence.h"
#include "CustomException.h"


template<typename T, template<class> class T2>
class DiagonalMatrix {
public:
    DiagonalMatrix();
    DiagonalMatrix(T *values, int length);
    ~DiagonalMatrix();
    void multiplyByNum(T value);
    void print();
    void set(int pos, T value);
    T operator[](int pos);
    int getSize();
    T getNorm();
    template<template<class> class T3>
    void add(DiagonalMatrix<T, T3> &term) {
        T tmp1, tmp2;
        if (size == term.getSize()) {
            for (int i = 0; i < size; i++) {
                tmp1 = (*data)[i];
                tmp2 = term[i];
                data->set(i, tmp1 + tmp2);
            }
        } else {
            throw DescriptiveException("SizeError: matrices must be the same size");
        }
    }
private:
    int size;
    Sequence<T> *data;
};


template<typename T, template<class> class T2>
DiagonalMatrix<T, T2>::DiagonalMatrix()
    : size(0), data(new T2<T>) {
}


template<typename T, template<class> class T2>
DiagonalMatrix<T, T2>::DiagonalMatrix(T *values, int length)
    : size(length), data(new T2<T>(values, length))
{
}


template<typename T, template<class> class T2>
DiagonalMatrix<T, T2>::~DiagonalMatrix() {
    delete data;
}


template<typename T, template<class> class T2>
inline void DiagonalMatrix<T, T2>::multiplyByNum(T value) {
    for (int i = 0; i < size; i++) {
        data->set(i, (*data)[i] * value);
    }
}

/*
template<typename T, template<class> class T2>
inline void DiagonalMatrix<T, T2>::add(DiagonalMatrix<T, T3> &term) {
    T tmp1, tmp2;
    if (size == term.getSize()) {
        if (this != &term) {
            for (int i = 0; i < size; i++) {
                tmp1 = (*data)[i];
                tmp2 = term[i];
                data->set(i, tmp1 + tmp2);
            }
        } else {
            for (int i = 0; i < size; i++) {
                tmp1 = (*data)[i] + (*data)[i];
                data->set(i, tmp1);
            }
        }
    } else {
        throw DescriptiveException("SizeError: matrices must be the same size");
    }
}*/


template<typename T, template<class> class T2>
inline void DiagonalMatrix<T, T2>::print() {
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (i == j) {
                std::cout << (*data)[i] << ' ';
            } else {
                std::cout << 0 << ' ';
            }
        }
        std::cout << std::endl;
    }
}


template<typename T, template<class> class T2>
inline void DiagonalMatrix<T, T2>::set(int pos, T value) {
    data->set(pos, value);
}


template<typename T, template<class> class T2>
inline T DiagonalMatrix<T, T2>::operator[](int pos) {
    return (*data)[pos];
}


template<typename T, template<class> class T2>
inline int DiagonalMatrix<T, T2>::getSize() {
    return size;
}


template<typename T, template<class> class T2>
inline T DiagonalMatrix<T, T2>::getNorm() {
    T tmp = 0;
    for (int i = 0; i < size; i++) {
        tmp += (*data)[i] * (*data)[i]  ;
    }
    return sqrt(tmp);
}


#endif
