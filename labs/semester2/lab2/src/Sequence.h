#ifndef LAB2_SEQUENCE_H
#define LAB2_SEQUENCE_H


#include <iostream>
#include "DynamicArray.h"
#include "LinkedList.h"


template<typename T>
class Sequence {
public:
    virtual void map(T (*funcPtr)(T)) = 0;
    virtual void insert(int pos, T value) = 0;
    virtual void remove(T value) = 0;
    virtual T pop(int pos = -1) = 0;
    virtual void print() = 0;
    virtual void set(int pos, T value) = 0;
    virtual T operator[](int pos) = 0;
    virtual int getLength() = 0;
    virtual void append(T value) = 0;
    virtual ~Sequence() = default;
};


#endif
