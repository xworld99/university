#ifndef SRC_CHAINHASHTABLE_H
#define SRC_CHAINHASHTABLE_H


#include "/home/egor/CLionProjects/LAB2_MAIN/src/Sequence.h"
#include "/home/egor/CLionProjects/lab3/src/TreeSet.h"
#include "/home/egor/CLionProjects/LAB2_MAIN/src/CustomException.h"
#include "HashTableCell.h"
#include <iostream>


#define STANDART_CAPACITY 10


template<typename TKey, typename TElement>
class ChainHashTable{
public:
    explicit ChainHashTable(int capacity_ = STANDART_CAPACITY);
    ChainHashTable(Sequence<TKey> keys, Sequence<TElement> elems);
    // make some constructors
    int getSize();
    int getCapacity();
    bool containsKey(TKey key);
    TElement get(TKey key);
    void clear();
    void print();
    void add(TKey key, TElement elem);
    void remove(TKey key);
    ~ChainHashTable();

    friend std::ostream& operator<< (std::ostream &out, ChainHashTable<TKey, TElement> &h);
private:
    int hash(TKey key);
    int capacity;
    int size;
    TreeSet<HashTableCell<TKey, TElement>> **data;
};


// make a good hash function
template<typename TKey, typename TElement>
int ChainHashTable<TKey, TElement>::hash(TKey key) {
    return key % capacity;
}


// i think it is ok
template<typename TKey, typename TElement>
ChainHashTable<TKey, TElement>::ChainHashTable(int capacity_)
: capacity(capacity_), size(0) {
    data = new TreeSet<HashTableCell<TKey, TElement>> * [capacity];
    for (int i = 0; i < capacity; i++) {
        data[i] = nullptr;
    }
}


template<typename TKey, typename TElement>
ChainHashTable<TKey, TElement>::ChainHashTable(Sequence<TKey> keys, Sequence<TElement> elems) : size(0) {
    int keysLen = keys.getLength();
    int elemsLen = elems.getLength();
    if (keysLen != elemsLen) {
        throw DescriptiveException("Error: arrays should have equals size");
    } else {
        capacity = keysLen;
        for (int i = 0; i < capacity; i++) {
            add(keys[i], elems[i]);
            size += 1;
        }
    }
}


template<typename TKey, typename TElement>
int ChainHashTable<TKey, TElement>::getSize() {
    return size;
}


template<typename TKey, typename TElement>
int ChainHashTable<TKey, TElement>::getCapacity() {
    return capacity;
}


template<typename TKey, typename TElement>
bool ChainHashTable<TKey, TElement>::containsKey(TKey key) {
    int pos = hash(key);
    if (data[pos]) {
        HashTableCell<TKey, TElement> el(key);
        return data[pos]->search(el);
    } else {
        throw DescriptiveException("Key error: this table isn`t contains this key");
    }
}


template<typename TKey, typename TElement>
TElement ChainHashTable<TKey, TElement>::get(TKey key) {
    int pos = hash(key);
    if (data[pos]) {
        HashTableCell<TKey, TElement> el(key);
        HashTableCell<TKey, TElement> res = data[pos]->get(el);
        return res.getElement();
    } else {
        throw DescriptiveException("Key error: this table isn`t contains this key");
    }
}


template<typename TKey, typename TElement>
void ChainHashTable<TKey, TElement>::clear() {
    for (int i = 0; i < capacity; i++) {
        if (data[i]) {
            data[i]->clear();
        }
    }
    delete data;
}


template<typename TKey, typename TElement>
void ChainHashTable<TKey, TElement>::print() {
    for (int i = 0; i < capacity; i++) {
        if (data[i]) {
            data[i]->saveToArray().print();
        }
    }
    std::cout << std::endl;
}


template<typename TKey, typename TElement>
void ChainHashTable<TKey, TElement>::add(TKey key, TElement elem) {
    int pos = hash(key);
    HashTableCell<TKey, TElement> el(key, elem);
    if (data[pos]) {
        data[pos]->insert(el);
    } else {
        data[pos] = new TreeSet<HashTableCell<TKey, TElement>>;
        data[pos]->insert(el);
    }
    size += 1;
}


template<typename TKey, typename TElement>
void ChainHashTable<TKey, TElement>::remove(TKey key) {
    int pos = hash(key);
    if (data[pos]) {
        HashTableCell<TKey, TElement> el(key);
        data[pos]->del(el);
    } else {
        throw DescriptiveException("Key error: this table isn`t contains this key");
    }
}


template<typename TKey, typename TElement>
ChainHashTable<TKey, TElement>::~ChainHashTable() {
    clear();
}


template<typename TKey, typename TElement>
std::ostream& operator<< (std::ostream &out, ChainHashTable<TKey, TElement> &h) {
    out << '{';
    for (int i = 0; i < h.capacity; i++) {
        if (h.data[i]) {
            DynamicArray<HashTableCell<TKey, TElement>> res = h.data[i]->saveToArray();
            out << res << ", ";
        }
    }
    out << '\b' << '\b' << "" << "";
    out << '}';
    return out;
}


#endif
