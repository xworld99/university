#include "ISorter.h"


template<typename T>
class BubbleSorter : public ISorter<T> {
public:
    Sequence<T> *sort(Sequence<T> *seq, bool (*cmp)(T, T)) override;
private:
    Sequence<T> *data;
};


template<typename T>
Sequence<T> * BubbleSorter<T>::sort(Sequence<T> *seq, bool (*cmp)(T, T)) {
    data = seq->clone();
    int len = data->getLength();
    for (int i = 0; i < len - 1; i++) {
        for (int j = 0; j < len - i - 1; j++) {
            if (cmp((*data)[j+1], (*data)[j])) {
                data->swap(j, j+1);
            }
        }
    }
    return data;
}