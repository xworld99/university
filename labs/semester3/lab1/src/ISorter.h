#ifndef S3LAB1_ISORTER_H
#define S3LAB1_ISORTER_H


#include <iostream>
#include </home/egor/CLionProjects/LAB2_MAIN/src/Sequence.h>


template<typename T>
class ISorter {
public:
    virtual Sequence<T> *sort(Sequence<T> *seq, bool (*cmp)(T, T)) = 0;
    virtual ~ISorter() = default;
};


#endif
