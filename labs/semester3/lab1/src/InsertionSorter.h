#include "ISorter.h"



template<typename T>
class InsertionSorter : public ISorter<T> {
public:
    Sequence<T> *sort(Sequence<T> *seq, bool (*cmp)(T, T)) override;
private:
    Sequence<T> *data;
    int binarySearch(T elem, int low, int high, bool (*cmp)(T, T));
};


template<typename T>
Sequence<T> * InsertionSorter<T>::sort(Sequence<T> *seq, bool (*cmp)(T, T)) {
    data = seq->clone();
    int i, loc, j;
    T selected;
    int len = seq->getLength();
    for (i = 1; i < len; ++i)
    {
        j = i - 1;
        selected = (*data)[i];
        loc = binarySearch(selected, 0, j, cmp);
        while (j >= loc)
        {
            data->set(j+1, (*data)[j]);
            j--;
        }
        data->set(j+1, selected);
    }
    return data;
}


template<typename T>
int InsertionSorter<T>::binarySearch(T elem, int low, int high, bool (*cmp)(T, T)) {
    if (high <= low)
        return (cmp((*data)[low], elem)) ? (low + 1) : low;
    int mid = (low + high) / 2;
    if(elem == (*data)[mid])
        return mid+1;
    if(elem > (*data)[mid])
        return binarySearch(elem, mid+1, high, cmp);
    return binarySearch(elem, low, mid-1, cmp);
}

