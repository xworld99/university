class User:
    def __init__(self, id, name, photo, sex, city='unspecified', all_content=0, all_comments=0):
        self.id = id
        self.name = name
        self.photo = photo
        self.sex = sex
        self.city = city
        self.all_content = all_content
        self.all_comments = all_comments
